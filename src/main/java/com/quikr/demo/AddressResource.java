
package com.quikr.demo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;

@Path("/addressAutoSuggestion")
public class AddressResource {

	private AddressService addressService = new AddressService();
	static Logger LOGGER = Logger.getLogger(AddressResource.class.getName());

	
	@GET
	@Produces("text/plain")
	public String getAutoSuggestion(@QueryParam("address") String address) {
		LOGGER.debug("Inside AddressResource.addressAutoSuggestion() address {} "+address);
		if(address != null)
		 return addressService.getAutoComplete(address, "IN").toString();
		
		return "Enter the address.";
	}
}
