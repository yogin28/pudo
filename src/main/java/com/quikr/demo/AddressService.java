package com.quikr.demo;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

public class AddressService {

	private static Trie trie = new Trie();
	static Logger LOGGER = Logger.getLogger(AddressResource.class.getName());

	public String getAutoComplete(String add, String country) {
		String address = add.toLowerCase();
		if (trie.autoComplete(address).isEmpty()) {
			List<String> list = GooglePlacesAutocompleteService.autocomplete(address, country);
			for (String works : list) {
				trie.insert(works);
			}
		}
		Iterator<String> it = trie.autoComplete(address).iterator();
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		while (it.hasNext()) {
			sb.append("\"");
			sb.append(it.next());
			sb.append("\"");
			if (it.hasNext())
				sb.append(",");
		}
		sb.append("]");
		LOGGER.info(sb);

		return sb.toString();
	}

}
