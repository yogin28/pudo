<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
</head>
<body>

	<div class="ui-widget">
		<label for="address">Enter Address : </label> <input id="tags">
	</div>

	<script>
		$(function() {

			$("#tags").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "/pudo-jersey/webresources/addressAutoSuggestion",
						dataType : "text",
						data : {
							address : request.term
						},
						success : function(data) {
							response(JSON.parse(data))
						}
					});
				},
				minLength : 3,
			});
		});
	</script>

</body>
</html>
